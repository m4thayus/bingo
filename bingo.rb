class Square
    
    attr_accessor :row, :column

end

def diagonal_bingo?(marks, dimension)
    n = 0
    m = 0

    marks.each do |square|
        case
        when square.row == square.column
            n += 1
        when square.row + square.column == (dimension - 1)
            m += 1
        end
    end
    n == dimension || m == dimension
end

def longitudinal_bingo?(marks, dimension)
    rows = []
    columns = []

    marks.each do |square|
        rows << square.row
        columns << square.column
    end
    c = 0
    for i in 0..(dimension - 1) do
        for j in 0..(dimension - 1) do
            case
            when rows.count(i) > c
                c = rows.count(i)
            when columns.count(j) > c
                c = columns.count(j)
            end
        end
    end
    uniq_rows = rows.uniq.length
    uniq_columns = columns.uniq.length

    c == dimension && (uniq_rows == dimension || uniq_columns == dimension)
end

def bingo?(marks, dimension)
    bingo = diagonal_bingo?(marks, dimension) || logitudinal_bingo?(marks, dimension)
end

def get_coords(dimension)
    coords = Array.new
    for i in 0..(dimension - 1)
        for j in 0..(dimension -1)
            coords << { x: i, y: j}
        end
    end
    return coords
end

def get_board(dimension)
    board = Array.new
    for i in 0..(dimension - 1)
        board[i] = Array.new
        for j in 0..(dimension - 1)
            board[i][j] = "   "
        end
    end
    board
end

def print_board(marks, dimension)
    board = get_board(dimension)

    marks.each do |square|
        row = square.row
        column = square.column
        board[row][column] = " X "
    end

    div = Array.new
    for i in 0..(dimension - 1)
        div[i] = "---"
    end

    puts ""
    for i in 0..(dimension - 1)  do  
        puts board[i].join("|")
        if i < dimension - 1
            puts div.join("|")
        end
    end
    puts ""
end

marks = []
print "Choose a dimension (n >= 3):"
dimension = gets.chomp.to_i
if dimension < 3 || !dimension.is_a?(Integer)
    dimension = 3
end
coords = get_coords(dimension)

until bingo?(marks, dimension)
    square = Square.new 
    
    index = (0..(coords.length - 1)).to_a.sample
    square.row = coords[index][:x].to_i
    square.column = coords[index][:y].to_i
    coords.delete_at(index)

    puts "Mark: (#{square.row}, #{square.column})"
    marks << square
end
print_board(marks, dimension)
